FROM amazoncorretto:11

ARG KAFKA_MIRROR=https://downloads.apache.org

ENV KAFKA_VERSION=2.5.0
ENV SCALA_VERSION=2.12
ENV PATH /opt/kafka/bin:$PATH

RUN mkdir -p /opt/kafka /etc/kafka
RUN echo

RUN echo "===> installing Kafka-${SCALA_VERSION}-${KAFKA_VERSION}..." \
    && yum -q -y update \
    && yum -q -y install tar gzip \
    && curl -s $KAFKA_MIRROR/kafka/$KAFKA_VERSION/kafka_$SCALA_VERSION-$KAFKA_VERSION.tgz | tar xz -C /opt/kafka --strip-components=1 \
    && echo "===> clean up ..."  \
    && yum -q -y remove tar gzip \
    && yum clean all \
    && rm -rf /tmp/* \
    && rm -rf /var/cache/yum

RUN cp /opt/kafka/config/server.properties /etc/kafka/server.properties
WORKDIR /opt/kafka

ENTRYPOINT ["bin/kafka-server-start.sh"]
CMD ["/etc/kafka/server.properties"]
